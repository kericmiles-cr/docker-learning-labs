const express = require('express')
    , app = express()
    , redis = require("redis")
    , process =require('process')
    , client = redis.createClient({
        'host': process.env.REDIS_HOST || '127.0.0.1'
    })
;
app.set('view engine', 'ejs');
app.use(express.static('public'))


app.get('/add', function(req, res) {
    client.set(req.query.value, '1', function(err, r) {
        res.redirect('/')
    })
})

app.get('/', function(req, res) {
    client.keys('*', function(err, items) {
        res.render('index', {
            'items': items || []
        })
    })

})

app.listen(3000, function () {
    console.log('App listening on port 3000!')
})